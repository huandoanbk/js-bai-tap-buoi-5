/**
 * Bài 1
 */
var diemChuan, khuVuc, doiTuong, diemMon1, diemMon2, diemMon3, tongDiem;

function tinhDiem() {
  diemChuan = document.getElementById("diemChuan").value * 1;
  khuVuc = document.getElementById("khuVuc").value;
  doiTuong = document.getElementById("doiTuong").value;
  diemMon1 = document.getElementById("diemMon1").value * 1;
  diemMon2 = document.getElementById("diemMon2").value * 1;
  diemMon3 = document.getElementById("diemMon3").value * 1;
  if (diemChuan < 0 || diemMon1 < 0 || diemMon2 < 0 || diemMon3 < 0) {
    // nhập điểm < 0
    alert("Điểm nhập không được âm!");
    var ketQua = document.getElementById("ketQua");
    ketQua.className = "";
    ketQua.innerText = "";
  } else if (diemMon1 == 0 || diemMon2 == 0 || diemMon3 == 0) {
    // điểm Liệt
    var ketQua = document.getElementById("ketQua");
    ketQua.className = "";
    ketQua.classList.add("text-danger");
    ketQua.innerText = "Bạn đã rớt. Do có môn điểm bằng 0";
  } // tính điểm
  else {
    tongDiem = diemMon1 + diemMon2 + diemMon3;
    switch (khuVuc) {
      case "A":
        tongDiem += 2;
        break;
      case "B":
        tongDiem += 1;
        break;
      case "C":
        tongDiem += 0.5;
        break;
      default:
        break;
    }

    switch (doiTuong) {
      case "1":
        tongDiem += 2.5;
        break;
      case "2":
        tongDiem += 1.5;
        break;
      case "3":
        tongDiem += 1;
        break;
      default:
        break;
    }

    if (tongDiem < diemChuan) {
      // Rớt
      var ketQua = document.getElementById("ketQua");
      ketQua.className = "";
      ketQua.classList.add("text-danger");
      ketQua.innerText = `Bạn đã rớt. Tổng điểm: ${tongDiem}`;
    } // đậu
    else {
      var ketQua = document.getElementById("ketQua");
      ketQua.className = "";
      ketQua.classList.add("text-success");
      ketQua.innerText = `Bạn đã đậu. Tổng điểm: ${tongDiem}`;
    }
  }
}

/**
 * Bài 2
 */
var hoTen, soKw, tienDien;
var muc1 = 500;
var muc2 = 650;
var muc3 = 850;
var muc4 = 1100;
var muc5 = 1300;

function tinhTiendien() {
  hoTen = document.getElementById("hoTen").value;
  soKw = document.getElementById("soKw").value * 1;
  tienDien = 0;
  if (soKw <= 0) {
    alert("Số Kw không hợp lệ! Vui lòng kiểm tra lại.");
    var soTien = document.getElementById("tienDien");
    soTien.innerText = "";
  } else if (soKw <= 50) {
    tienDien = soKw * muc1;
  } else if (soKw > 50 && soKw <= 100) {
    tienDien = 50 * muc1 + (soKw - 50) * 650;
  } else if (soKw > 100 && soKw <= 200) {
    tienDien = 50 * muc1 + 50 * muc2 + (soKw - 100) * muc3;
  } else if (soKw > 200 && soKw <= 350) {
    tienDien = 50 * muc1 + 50 * muc2 + 100 * muc3 + (soKw - 350) * muc4;
  } else if (soKw > 350) {
    tienDien =
      50 * muc1 + 50 * muc2 + 100 * muc3 + 150 * muc4 + (soKw - 350) * 1300;
  }
  var soTien = document.getElementById("tienDien");
  soTien.innerText = `Họ tên: ${hoTen}. Tiền điện: ${new Intl.NumberFormat().format(
    tienDien
  )} VNĐ`;
}
